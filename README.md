# Readme

## Auth

Auth is provided by a token sent from the client.

The token is used to generate a user object on the context for ease of access and maintenance.

Each query and mutation then determines based on user object if it can be called. This allows for a single url to handle both public and private requests.

### AUTH0_API_IDENTIFIER

Unique identifier for the AUTH0 API. This value will be used as the audience parameter on authorization calls and is the same as the client id when connecting from a client. For machine to machine calls there is a different api identifier i.e. AUTH0_API_IDENTIFIER=https://woodford.eu.auth0.com/api/v2/

## Links

### Zeit Now

- [Environments](https://blog.echobind.com/up-and-running-zeit-now-environments-6eae4b689bb)

### Prisma

- [Generate Schema for importing](https://github.com/Urigo/graphql-cli/issues/354)
- [Importing schema](https://www.prisma.io/forum/t/different-method-of-importing-content-types-into-schema-graphql/3475)

### Apollo Server

- [Apollo Server Auth](https://www.apollographql.com/docs/apollo-server/security/authentication/)

### GraphQL

- [Handling Errors in GraphQL](https://dev.to/andre/handling-errors-in-graphql--2ea3)

### Auth0

#### Roy Derks

- [Build and secire a graphql server with nodejs](https://auth0.com/blog/build-and-secure-a-graphql-server-with-node-js/)
