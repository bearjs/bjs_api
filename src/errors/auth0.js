const {createError} = require('apollo-errors');
const forEach = require('lodash/forEach');

const auth0ErrorTypes = {
  TokenNotFoundError: {
    message: 'No jwt token was found. Please login with your credentials.',
  },
  TokenInvalidError: {
    message: 'The jwt token is invalid. Please login with the correct credentials.',
  },
  TokenExpiredError: {
    message: 'The given jwt token has expired. Please login again to connect to the API.',
  },
};

const auth0Errors = {};

forEach(auth0ErrorTypes, ({message}, key) => {
  auth0Errors[key] = createError(key, {message});
});

module.exports = {
  auth0Errors,
  auth0ErrorTypes,
};
