// Connect to remote Prisma DB and allows us to query it with js
const {PrismaClient} = require('@prisma/client');
const prisma = new PrismaClient();

module.exports = prisma;
