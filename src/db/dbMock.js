const {MongoMemoryServer} = require('mongodb-memory-server');
const {MongoClient} = require('mongodb');

let database = null;

const startDatabase = async () => {
  if (!database) {
    const mongo = new MongoMemoryServer();
    const mongoDBURL = await mongo.getUri();
    let connection;
    try {
      connection = await MongoClient.connect(mongoDBURL, {
        useUnifiedTopology: true,
        useNewUrlParser: true,
      });
      console.log('DB connected');
    } catch (ex) {
      console.log(`DB Connection Error: ${ex.message}`);
    }

    database = connection.db();

    await database.collection('events').insertMany([
      {
        id: 1,
        title: 'All About BearJS',
        description:
          'Introduction event to discuss the benefits of using the BearJS Modular App for SMEs',
        date: '2019-11-06T17:34:25+00:00',
        attendants: [
          {
            id: 1,
            name: 'Peter Keating',
            email: 'peter.keating@bearjs.co.uk',
          },
          {
            id: 2,
            name: 'Howard Roark',
            email: 'howard.roark@bearjs.co.uk',
          },
        ],
      },
      {
        id: 2,
        title: 'All About BearJS #2',
        description:
          'Introduction event to discuss the benefits of using the BearJS Modular App for SMEs',
        date: '2019-11-06T17:34:25+00:00',
        attendants: [
          {
            id: 1,
            name: 'John Galt',
            email: 'john.galt@bearjs.co.uk',
          },
        ],
      },
    ]);
  }

  return database;
};

module.exports = startDatabase;
