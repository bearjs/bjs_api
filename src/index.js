const {GraphQLServer} = require('graphql-yoga');
const {formatError} = require('apollo-errors');
const resolvers = require('./resolvers');
const startDatabase = require('./db/dbMock');
const prisma = require('./db/prisma');
const {getUser} = require('./auth');
const {API_PORT: port} = process.env;

const server = new GraphQLServer({
  typeDefs: './src/app.graphql',
  resolvers,
  opts: {
    port,
  },
  context: async req => {
    // contextual data
    const db = await startDatabase();
    const token = req.request.get('Authorization');
    const user = await getUser(token);
    console.log(`-------------- ${new Date().toISOString()}`, user);
    return {
      ...req,
      token,
      user,
      db,
      prisma,
    };
  },
});

const options = {
  cors: {
    credentials: true,
    origin: process.env.CLIENT_URL,
  },
  formatError,
};

const startServer = () => {
  server.start(options, () =>
    console.log(`😄 Server running at http://localhost:${port}`)
  );
};

module.exports = startServer;
