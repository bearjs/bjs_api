const hello = (_, {name}) => `Hello ${name || 'World'}`;

const dinnerOptions = ['🍕', '🌭', '🍔', '🥗', '🍣'];

const whatsForDinner = () => {
  const idx = Math.floor(Math.random() * dinnerOptions.length);
  const foodChoice = dinnerOptions[idx];
  return `Tonight we eat ${foodChoice}`;
};

const events = async (parent, args, ctx) => {
  const {db, user} = ctx;
  let events = await db.collection('events').find();

  if (user.error) {
    //public query so fail silently but hide sensitive data
    events = events.project({attendants: null});
  }

  return events.toArray();
};

const event = async (parent, {id}, ctx) => {
  const {db, user} = ctx;
  let event = await db.collection('events').findOne({id});

  if (user.error) {
    //public query so fail silently but hide sensitive data
    event = {...event, attendants: null};
  }

  return event;
};

const user = async (parent, args, ctx) => {
  const {user} = ctx;

  if (user.error) {
    const {GQLError} = user.error;

    throw new GQLError();
  }

  let userFromPrisma = await ctx.prisma.user.findOne({
    where: {email: user.email},
    include: {
      posts: true,
      profile: true,
      addresses: true,
    },
  });

  console.dir({userFromPrisma}, {depth: null});

  if (!userFromPrisma) {
    //No user found which means first login with email, create user profile in db
    const {email, nickname} = user;
    userFromPrisma = await ctx.prisma.user.create({
      data: {
        nickname,
        email: email.toLowerCase(),
        permissions: {set: ['USER']},
        posts: {
          create: {title: 'Hello World'},
        },
        profile: {
          create: {bio: 'I like...'},
        },
      },
    });
  }

  const {
    nickname,
    name,
    firstName,
    lastName,
    email,
    id,
    permissions,
    addresses,
    profile,
  } = userFromPrisma;
  return {
    id,
    nickname: nickname || '',
    name,
    lastName: lastName || '',
    firstName: firstName || '',
    email,
    emailVerified: user.email_verified,
    permissions,
    addresses,
    profile: {
      bio: profile.bio || '',
    },
  };
};

const dividendVouchers = async (parent, args, ctx, info) => {
  const {user} = ctx;

  if (user.error) {
    const {GQLError} = user.error;

    throw new GQLError();
  }

  const dividendVouchers = await ctx.prisma.dividendVoucher.findMany(
    {
      where: {
        createdBy: {
          email: user.email,
        },
      },
      include: {
        createdBy: true,
      },
    },
    info
  );

  console.log(dividendVouchers);
  return dividendVouchers;
};

module.exports = {
  hello,
  whatsForDinner,
  events,
  event,
  user,
  dividendVouchers,
};
