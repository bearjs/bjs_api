const editEvent = async (parent, {id, title, description}, ctx) => {
  const {db, user} = ctx;

  if (user.error) {
    const {GQLError} = user.error;

    throw new GQLError();
  }

  const {value} = await db
    .collection('events')
    .findOneAndUpdate({id}, {$set: {title, description}}, {returnOriginal: false});

  return value;
};

const updateUser = async (parent, args, ctx) => {
  const {prisma, user} = ctx;

  if (user.error) {
    const {GQLError} = user.error;

    throw new GQLError();
  }

  const {firstName, lastName, nickname, bio} = args.userInput;

  const updatedUser = await prisma.user.update({
    where: {email: user.email},
    include: {profile: true},
    data: {
      firstName,
      lastName,
      nickname,
      profile: {
        update: {
          bio,
        },
      },
    },
  });

  return updatedUser;
};

const createDividendVoucher = async (parent, args, ctx) => {
  const {prisma, user} = ctx;

  if (user.error) {
    const {GQLError} = user.error;

    throw new GQLError();
  }

  const {
    dividendVoucherCreateInput: {
      shareholder,
      company,
      meetingAndDeclarationDate,
      meetingAttendees,
      meetingLocation,
      isFinal,
      dividendPerShare,
      companyYearEndDate,
      numberOfSharesOwned,
    },
  } = args;

  const createDividendVoucher = await prisma.dividendVoucher.create({
    data: {
      createdBy: {
        connect: {
          email: user.email,
        },
      },
      shareholder,
      company,
      meetingAndDeclarationDate,
      meetingAttendees: {
        set: meetingAttendees,
      },
      meetingLocation,
      isFinal,
      dividendPerShare,
      companyYearEndDate,
      numberOfSharesOwned,
    },
  });

  return createDividendVoucher;
};

const deleteDividendVouchers = async (parent, args, ctx) => {
  const {prisma, user} = ctx;

  if (user.error) {
    const {GQLError} = user.error;

    throw new GQLError();
  }

  const {ids} = args;

  const {count} = await prisma.dividendVoucher.deleteMany({
    where: {
      id: {
        in: ids.map(id => parseInt(id, 10)),
      },
    },
  });

  return {
    message: `${count} item(s) successfully deleted. Ids as follows: ${ids.join(', ')}`,
  };
};

const createAddress = async (parent, {addressCreateInput}, ctx) => {
  const {prisma, user} = ctx;

  if (user.error) {
    const {GQLError} = user.error;

    throw new GQLError();
  }

  console.log({addressCreateInput});
  const createDividendVoucher = await prisma.address.create({
    data: {
      ...addressCreateInput,
      user: {
        connect: {email: user.email},
      },
    },
  });

  return createDividendVoucher;
};

const deleteAddresses = async (parent, args, ctx) => {
  const {prisma, user} = ctx;

  if (user.error) {
    const {GQLError} = user.error;

    throw new GQLError();
  }

  const {ids} = args;

  const {count} = await prisma.address.deleteMany({
    where: {
      id: {
        in: ids.map(id => parseInt(id, 10)),
      },
    },
  });

  return {
    message: `${count} item(s) successfully deleted. Ids as follows: ${ids.join(', ')}`,
  };
};

module.exports = {
  editEvent,
  updateUser,
  createDividendVoucher,
  deleteDividendVouchers,
  createAddress,
  deleteAddresses,
};
