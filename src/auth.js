//https://auth0.com/blog/develop-modern-apps-with-react-graphql-apollo-and-add-authentication/#Secure-your-GraphQL-API-with-Auth0
const jwt = require('jsonwebtoken');
const jwksClient = require('jwks-rsa');
const {auth0Errors} = require('./errors/auth0');
const {AUTH0_DOMAIN, AUTH0_API_IDENTIFIER} = process.env;

const client = jwksClient({
  jwksUri: `https://${AUTH0_DOMAIN}/.well-known/jwks.json`,
});

const getKey = (header, callback) => {
  client.getSigningKey(header.kid, function(err, key) {
    if (!key) {
      return;
    }
    const signingKey = key.publicKey || key.rsaPublicKey;
    callback(null, signingKey);
  });
};

const options = {
  audience: AUTH0_API_IDENTIFIER,
  issuer: `https://${AUTH0_DOMAIN}/`,
  algorithms: ['HS256', 'RS256'],
};

// simple auth check on every request
async function getUserDetailsFromToken(token) {
  const bearerToken = token.split(' ');

  return new Promise((resolve, reject) => {
    jwt.verify(bearerToken[1], getKey, options, (ex, decoded) => {
      if (ex) {
        reject(ex);
      }
      resolve(decoded);
    });
  });
}

async function getUser(token) {
  let user = {};

  if (!token) {
    user.error = {
      name: 'TokenNotFoundError',
      GQLError: auth0Errors('TokenNotFoundError'),
    };
  }

  try {
    user = await getUserDetailsFromToken(token);
  } catch (ex) {
    const {name} = ex;

    user.error = {
      name,
      GQLError: auth0Errors[ex.name],
    };
  }

  return user;
}

module.exports = {getUser};
