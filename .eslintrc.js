const OFF = 0;
const WARN = 'warn';
const ERROR = 'error';

module.exports = exports = {
  env: {
    es6: true,
    browser: true,
  },

  settings: {
    version: 'detect',
  },

  parserOptions: {
    sourceType: 'module',
    ecmaVersion: 2020,
  },

  extends: [
    'eslint:recommended',
    'plugin:node/recommended',
    'plugin:prettier/recommended',
    'plugin:json/recommended',
  ],

  plugins: ['react'],

  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
    window: true,
    test: true,
    require: true,
    exports: true,
    expect: true,
    module: true,
    process: true,
    __PATH_PREFIX__: true,
    __dirname: true,
  },

  rules: {
    indent: [ERROR, 2],
    'linebreak-style': [ERROR, 'unix'],
    quotes: [ERROR, 'single'],
    semi: [ERROR, 'always'],
    'node/exports-style': [ERROR, 'module.exports'],
    'node/file-extension-in-import': [ERROR, 'always'],
    'node/prefer-global/buffer': [ERROR, 'always'],
    'node/prefer-global/console': [ERROR, 'always'],
    'node/prefer-global/process': [ERROR, 'always'],
    'node/prefer-global/url-search-params': [ERROR, 'always'],
    'node/prefer-global/url': [ERROR, 'always'],
    'node/prefer-promises/dns': ERROR,
    'node/prefer-promises/fs': ERROR,
  },
};
